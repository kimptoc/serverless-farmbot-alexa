'use strict';

const dashbot = require('dashbot')(process.env.DASHBOT_API_KEY).alexa;

var Alexa = require('alexa-sdk');

var APP_ID = "amzn1.ask.skill.165f3e91-b3b6-4b46-a2aa-3d0fd2f85691"; //OPTIONAL: replace with "amzn1.echo-sdk-ams.app.[your-unique-value-here]";

var states = [
    'hello',
    'toggle_led',
    'move_forward',
    'move_back',
    'move_home',
    'cancel',
    'help',
    'stop',
    'move_left',
    'move_right',
    'move_up',
    'move_down',
    'mount_weeder',
    'mount_waterer',
    'mount_sensor',
    'move_absolute',
    'move_absolute_percent',
    'move_relative',
    'move_relative_direction',
    'take_picture',
    'not_implemented',
    'unhandled'
];

exports.farmbot = dashbot.handler(function(event, context, callback) {
    var handlers = {};
    states.forEach(function(state) {
        var intent_handler = require('./intents/'+state);
        var intent_name = intent_handler[0];
        var intent_function = intent_handler[1];
        console.log("Setting up intent:"+state+"/"+intent_name);
        handlers[intent_name] = function() {
          intent_function(this);
        };
    });
    var alexa = Alexa.handler(event, context);
    alexa.APP_ID = APP_ID;
    alexa.registerHandlers(handlers);
    alexa.execute();
});



