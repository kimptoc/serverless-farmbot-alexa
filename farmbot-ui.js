"use strict";

var request = require('request');

(function () {

    // var root = this;

    function FarmbotUI() {}

    FarmbotUI.prototype.login = function(user_email, passwd, callback) {
        console.log("login:"+user_email);
        var options = {
            method: 'POST',
            url:"https://my.farmbot.io/api/tokens",
            body: {user: {email: user_email, password: passwd}},
            json: true,
            headers: {
                
            }
        }
        request(options, function(error, response, body){
            console.log("error");
            console.log(JSON.stringify(error));
            console.log("body");
            console.log(JSON.stringify(body))
            console.log(JSON.stringify(body.error))
          if (error || body.error){
            console.log("error");
            // console.log(JSON.stringify(error));
            if (!error) error = body.error;
            callback(error, null);
          } else {
            console.log("ok");
            // console.log(JSON.stringify(body))
            var MY_SHINY_TOKEN = body.token.encoded;
            callback(null, {token:MY_SHINY_TOKEN, raw_response:response});
          }
        })
    }
    
    FarmbotUI.prototype.device = function(token, callback) {
        console.log("device/token:"+token);
        var options = {
            method: 'GET',
            url:"https://my.farmbot.io/api/device",
            // body: {user: {email: user_email, password: passwd}},
            json: true,
            headers: {
               "Authorization" : token
            }
        }
        request(options, function(error, response, body){
            console.log("error");
            console.log(JSON.stringify(error));
            console.log("body");
            console.log(JSON.stringify(body))
            console.log(JSON.stringify(body.error))
          if (error || body.error){
            console.log("error");
            console.log(JSON.stringify(error));
            if (!error) error = body.error;
            callback(error, null);
          } else {
            console.log("ok");
            console.log(JSON.stringify(body))
            callback(null, {info:body});
          }
        })
    }
    
//    FarmbotUI.prototype.login = function(user_email, passwd, callback) {
    //   $.ajax({
        //   url: "https://my.farmbot.io/api/tokens",
        //   type: "POST",
        //   data: JSON.stringify({user: {email: user_email, password: passwd}}),
        //   contentType: "application/json",
        //   success: function (data) {
                   // You can now use your token:
                //   var MY_SHINY_TOKEN = data.token.encoded;
                //   callback(null, {token:MY_SHINY_TOKEN, raw_response:data});
            //   },
        //   error: function( jqXHR, textStatus, errorThrown ) {
            //   callback(jqXHR.responseText, null);
        //   }
    //   });
    // }


    // FarmbotUI.prototype.sequences = function(token, callback){
        // $.ajax({
            // url: "https://my.farmbot.io/api/sequences",
            // type: "GET",
            // contentType: "application/json",
            // beforeSend: function(request) {
            //   request.setRequestHeader("Authorization", token);
            // },
            // success: function (data) {
                    //   You can now use your token:
                    //  callback(null, {sequences:data});
                //  },
            // error: function( jqXHR, textStatus, errorThrown ) {
                // callback(jqXHR.responseText, null);
            // }
        // })
    // }

    // FarmbotUI.prototype.images = function(token, callback){
        // $.ajax({
            // url: "https://my.farmbot.io/api/images",
            // type: "GET",
            // contentType: "application/json",
            // beforeSend: function(request) {
            //   request.setRequestHeader("Authorization", token);
            // },
            // success: function (data) {
                    //   You can now use your token:
                    //  callback(null, {images:data});
                //  },
            // error: function( jqXHR, textStatus, errorThrown ) {
                // callback(jqXHR.responseText, null);
            // }
        // })
    // }

    // root.FarmbotUI = FarmbotUI;
    module.exports = FarmbotUI;

})();

