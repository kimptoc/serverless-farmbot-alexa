var fn = require('../functions');
var Constants = require("../constants");

module.exports = [
    'MoveAbsoluteIntent', function(alexa) {
        // alexa.emit('NotImplemented');
                console.log(alexa.event.request.intent.slots);
        var x = 0;
        var y = 0;
        var z = 0;
        if (alexa.event.request.intent.slots.x.value)
          x = parseInt(alexa.event.request.intent.slots.x.value);
        if (alexa.event.request.intent.slots.y.value)
          y = parseInt(alexa.event.request.intent.slots.y.value);
        if (alexa.event.request.intent.slots.z.value)
          z = parseInt(alexa.event.request.intent.slots.z.value);
        fn.moveAbsolute(x, y, z, function(){
          console.log("move callback");

          var speech = "Farmbot has moved to "+x+", by "+y+", by "+z;
          alexa.emit(':tellWithCard', speech, Constants.SKILL_NAME, speech);

        });

    }
]
