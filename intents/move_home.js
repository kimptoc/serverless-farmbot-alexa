
var fn = require('../functions');
var Constants = require("../constants");

module.exports = ['MoveHomeIntent', function(alexa){

        // var alexa = this;
        console.log("farmbot has been contacted!");
        fn.moveAbsolute(0,0,0, function(){
          console.log("move home callback");

          var speech = "Farmbot has moved to home location";
          alexa.emit(':tellWithCard', speech, Constants.SKILL_NAME, speech);

        });

}
]