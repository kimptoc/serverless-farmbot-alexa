var fn = require('../functions');
var Constants = require("../constants");

module.exports = [
    'MoveRelativeDirectionIntent', function(alexa) {
        // alexa.emit('NotImplemented');
        console.log(alexa.event.request.intent.slots);
        var max_x = 2800;
        var max_y = 1300;
        var max_z = 440;
        var x = 0; 
        var y = 0;
        var z = 0;
        var amount = 0;
        var direction = '';
        if (alexa.event.request.intent.slots.amount.value)
          amount = parseFloat(alexa.event.request.intent.slots.amount.value);
        if (alexa.event.request.intent.slots.direction.value)
          direction = alexa.event.request.intent.slots.direction.value;
        switch (direction){
          case 'up':
            z = -amount;
            break;
          case 'down':
            z = amount;
            break;
          case 'right':
          case 'right side':
            y = amount;
            break;
          case 'left':
          case 'left side':
            y = -amount;
            break;
          case 'backwards':
          case 'backward':
          case 'back':
            x = -amount;
            break;
          case 'forwards':
          case 'forward':
            x = amount;
            break;
        }
        fn.moveRelative(x, y, z, function(){
          console.log("move callback");

          var speech = "Farmbot has moved "+x+", by "+y+", by "+z;
          alexa.emit(':tellWithCard', speech, Constants.SKILL_NAME, speech);

        });

    }
]
