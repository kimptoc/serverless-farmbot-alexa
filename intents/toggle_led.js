
var fn = require('../functions');
var Constants = require("../constants");

module.exports = ['ToggleLedIntent', function(alexa){

        console.log("farmbot has been contacted!");
        fn.toggleLed(function(){
          console.log("login callback");

         var speech = "Farmbot LED has been toggled";
         alexa.emit(':tellWithCard', speech, Constants.SKILL_NAME, speech);

        });

}
]