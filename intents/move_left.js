
var fn = require('../functions');
var Constants = require("../constants");

module.exports = ['MoveLeftIntent', function(alexa){

        console.log("farmbot has been contacted!");
        var y = 100;
        if (alexa.event.request.intent.slots.y.value)
          y = parseInt(alexa.event.request.intent.slots.y.value);

        fn.moveRelative(0, -y, 0, function(){
          console.log("move callback");

          var speech = "Farmbot has moved left "+y;
          alexa.emit(':tellWithCard', speech, Constants.SKILL_NAME, speech);

        });

}
]