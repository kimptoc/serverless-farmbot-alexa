var fn = require('../functions');
var Constants = require("../constants");

module.exports = [
    'TakePictureIntent', function(alexa) {
        // alexa.emit('NotImplemented');
        
        console.log("farmbot has been contacted!");
        fn.takePhoto(function(){
          console.log("photo callback");

          var speech = "Farmbot has taken a photo";
          alexa.emit(':tellWithCard', speech, Constants.SKILL_NAME, speech);

        });

    }
]
