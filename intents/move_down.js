var fn = require('../functions');
var Constants = require("../constants");

module.exports = [
    'MoveDownIntent', function(alexa) {
        // alexa.emit('NotImplemented');
        console.log(alexa.event.request.intent.slots);
        var z = 100;
        if (alexa.event.request.intent.slots.z.value)
          z = parseInt(alexa.event.request.intent.slots.z.value);
        fn.moveRelative(0, 0, z, function(){
          console.log("move callback");

          var speech = "Farmbot has moved down "+z;
          alexa.emit(':tellWithCard', speech, Constants.SKILL_NAME, speech);

        });
    }
]
