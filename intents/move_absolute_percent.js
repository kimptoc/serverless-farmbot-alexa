var fn = require('../functions');
var Constants = require("../constants");

module.exports = [
    'MoveAbsolutePercentIntent', function(alexa) {
        // alexa.emit('NotImplemented');
        console.log(alexa.event.request.intent.slots);
        var max_x = 2800;
        var max_y = 1300;
        var max_z = 440;
        var x = 0; 
        var y = 0;
        var z = 0;
        var percent = 0;
        var absolute_direction = '';
        if (alexa.event.request.intent.slots.percent.value)
          percent = parseFloat(alexa.event.request.intent.slots.percent.value);
        if (alexa.event.request.intent.slots.absolute_direction.value)
          absolute_direction = alexa.event.request.intent.slots.absolute_direction.value;
        console.log('MoveAbsolutePercentIntent:'+percent+' '+absolute_direction);
        switch (absolute_direction){
          case 'down':
            z = (max_z * percent) / 100;
            break;
          case 'across':
            y = (max_y * percent) / 100;
            break;
          case 'along':
            x = (max_x * percent) / 100;
            break;
        }
        fn.moveRelative(x, y, z, function(){
          console.log("move callback");

          var speech = "Farmbot has moved to "+x+", by "+y+", by "+z;
          alexa.emit(':tellWithCard', speech, Constants.SKILL_NAME, speech);

        });

    }
]
