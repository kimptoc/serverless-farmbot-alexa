
var fn = require('../functions');
var Constants = require("../constants");

module.exports = ['MoveBackIntent', function(alexa){

        console.log("farmbot has been contacted!");
        var x = 100;
        if (alexa.event.request.intent.slots.x.value)
          x = parseInt(alexa.event.request.intent.slots.x.value);

        fn.moveRelative(-x, 0, 0, function(){
          console.log("move callback");

          var speech = "Farmbot has moved back "+x;
          alexa.emit(':tellWithCard', speech, Constants.SKILL_NAME, speech);

        });

}
]