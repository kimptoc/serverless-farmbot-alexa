var fn = require('../functions');

module.exports = [
    'HelloIntent', function(alexa) {

        fn.device(function(err, farmbot, farmbotUI, device){
          console.log("device callback");
          console.log(device);

          alexa.emit(':tell', 'Farmbot '+device.info.name+' is ready to go!');

        });


    }
]
