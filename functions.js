'use strict';

// Returns a random integer between min (inclusive) and max (inclusive)
// const getRandomInt = (min, max) => Math.floor(Math.random() * ((max - min) + 1)) + min;

var farmbot_ui = require('./farmbot-ui.js');
global.atob = require("atob");
var Farmbot = require('farmbot');

module.exports.device = (callback) => {
  farmbot_prep(function(error, farmbot, farmbotUI, device){
    callback(error, farmbot, farmbotUI, device);
  });
};

module.exports.takePhoto = (callback) => {
  farmbot_prep(function(error, farmbot){
        farmbot
          .takePhoto({}) 
          .then(function (bot) {
            console.log("Photo taken.");
            callback();
          })
          .catch(function (err) {
            console.log("Failed to take photo:"+JSON.stringify(err));
            callback();
          });
  });
};

module.exports.toggleLed = (callback) => {
  farmbot_prep(function(error, farmbot){
        farmbot
          .togglePin({pin_number: 13})
          .then(function (bot) {
            console.log("Toggled pin.");
            callback();
          })
          .catch(function (err) {
            console.log("Failed to toggle pin:"+JSON.stringify(err));
            callback();
          });
  });
};

module.exports.moveRelative = (distance_x, distance_y, distance_z, callback) => {
  farmbot_prep(function(error, farmbot){
    farmbot.moveRelative({x:distance_x, y:distance_y, z:distance_z})
          .then(function (bot) {
            console.log("Move relative.");
            callback();
          })
          .catch(function (err) {
            console.log("Failed to move relative:"+JSON.stringify(err));
            callback();
          });
  });
};

module.exports.moveAbsolute = (x, y, z, callback) => {
  farmbot_prep(function(error, farmbot){
    farmbot.moveAbsolute({x:x, y:y, z:z})
          .then(function (bot) {
            console.log("Move absolute.");
            callback();
          })
          .catch(function (err) {
            console.log("Failed to move absolute:"+JSON.stringify(err));
            callback();
          });
  });
};

function farmbot_prep(callback) {
  // const upperLimit = event.request.intent.slots.UpperLimit.value || 100;
  // const number = getRandomInt(0, upperLimit);

  var fb = new farmbot_ui();
  fb.login(process.env.fb_user,process.env.fb_pass, function(err, info){
    if (err) {
      console.log("err:"+JSON.stringify(err));
      callback(err, null, fb);
    } else {
    var login_info = info.token;
    console.log("token:"+login_info);
    var farmbot = new Farmbot.Farmbot({token: login_info, timeout: 30000});
//    farmbot.setState("timeout", 999);
    farmbot.connect()
      .then(function(){
        console.log("connected/1!");
        fb.device(login_info, function(err, device){ 
          console.log(device);
          callback(err, farmbot, fb, device);
        });
    })
      // .then(function(){
        // console.log("connected/2!");
        // callback(null, farmbot, fb);
    // })
    .catch(function(err){
      console.log("Error:"+JSON.stringify(err));
      callback(err, null, fb);
    });
//    callback();
    }
  });
}
